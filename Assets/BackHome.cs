using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackHome : MonoBehaviour
{
  public void returnHome()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}
