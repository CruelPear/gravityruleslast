using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class playerController : MonoBehaviour
{
    Rigidbody2D body;

    float horizontal;
    float vertical;
    public bool moveHorizontal;
    public Joystick joystick;

    Vector2 nine = new Vector2(-9.8f, 0.0f);
    Vector2 twelve = new Vector2(0.0f, 9.8f);
    Vector2 three = new Vector2(9.8f, 0.0f);
    Vector2 six = new Vector2(0.0f, -9.8f);
    public Vector2 checkpoint;
    public float jumpForce;
    private Quaternion playerRotation;

    public float runSpeed = 10.0f;

    private Animator anim;
    private SpriteRenderer theSR;

    public GameObject canvas;
    public int noOfTurns;
    public Image gravityButton;
    private float timer = 0.0f;
    public float gravityCoolDown = 3f;
    private bool canGravity = true;
    private int number;
    public float time = 100;
    public static float timeCopy;
    public static bool adsWatched;
    private float startTime;

    public Button button;

    private void Awake()
    {
        //hi chamika! I think you should put the dontdestroy on load function here :)
        //joystick = GetComponent<Joystick>();
        //joystick = GameObject.FindGameObjectWithTag("Joystick");

        //Joystick.DontDestroyOnLoad(joystick);

        // joystick = (Joystick) Resources.Load("Assets/Proto-Prefabs/Canvas.prefab");
        //DontDestroyOnLoad(canvas);
    }

    // Start is called before the first frame update
    void Start()
    {
        startTime = time;
        body = GetComponent<Rigidbody2D>();
        moveHorizontal = true;
        Physics2D.gravity = six;
        checkpoint = transform.position;
        playerRotation = transform.rotation;

        anim = GetComponent<Animator>();
        theSR = GetComponent<SpriteRenderer>();
        GameoverScript.instance.gameObject.SetActive(false);

        number = 100;
        adsWatched = false;
        timeCopy = time;
    }
     
    // Update is called once per frame
    void Update()
    {
        timeCopy = time;
        //Debug.Log(timeCopy);
        if (adsWatched) {
            gameOverUNO();
            adsWatched = false;
        }
        if(time > 0)
        {
            time -= Time.deltaTime; ;
            if ((int)time != number)
            {
                number = (int)time;
            }
        }
        else {
            gameOver();
        }

        timer += Time.deltaTime;
        //if(noOfTurns > 0)
        if(true)
        {
            //GameoverScript.instance.gameObject.SetActive(false);
            rotatePlayer();
            // horizontal = Input.GetAxisRaw("Horizontal"); //Use keyboard or console controls
            // vertical = Input.GetAxisRaw("Vertical");
            horizontal = joystick.Horizontal; //Use virtual joystick controls with walk and run
            vertical = joystick.Horizontal;
            if (joystick.Horizontal >= .2f)
            {
                horizontal = 1f;
            }
            else if (joystick.Horizontal <= -.2f)
            {
                horizontal = -1f;
            }
            else
            {
                horizontal = 0f;
            }

            if (joystick.Vertical >= .2f)
            {
                vertical = 1f;
            }
            else if (joystick.Vertical <= -.2f)
            {
                vertical = -1f;
            }
            else
            {
                vertical = 0f;
            }

            //  changeGravity(); //uncomment this and uncomment if statement inside the function.commented due to button being activated inside jump button.

            if ((moveHorizontal & (vertical != 0)) || (!moveHorizontal & (horizontal != 0)))
            {
                if (moveHorizontal)
                {
                    if (vertical > 0 & Physics2D.gravity == six)
                    {
                        body.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);


                    }
                    else if (vertical < 0 & Physics2D.gravity == twelve)
                    {
                        body.AddForce(new Vector2(0, -jumpForce), ForceMode2D.Impulse);


                    }
                }
                else
                {
                    if (horizontal > 0 & Physics2D.gravity == nine)
                    {
                        body.AddForce(new Vector2(jumpForce, 0), ForceMode2D.Force);


                    }
                    else if (horizontal < 0 & Physics2D.gravity == three)
                    {
                        body.AddForce(new Vector2(-jumpForce, 0), ForceMode2D.Force);


                    }
                }
            }

            /*if (Physics2D.gravity == nine) { 
                if()
            }*/
        }
       /* else
        {
            joystick.gameObject.SetActive(false);
            gravityButton.gameObject.SetActive(false);
            GameoverScript.instance.gameObject.SetActive(true);
            Debug.Log("GAME OVER");
        }*/

    }

    public void gameOver()
    {
        joystick.gameObject.SetActive(false);
        gravityButton.gameObject.SetActive(false);
        GameoverScript.instance.gameObject.SetActive(true);
    }

    public void gameOverUNO()
    {
        joystick.gameObject.SetActive(true);
        gravityButton.gameObject.SetActive(true);
        GameoverScript.instance.gameObject.SetActive(false);
        time = startTime;
    }

    public void changeGravity()
    {
        //if (Input.GetKeyDown(KeyCode.F) )
        //{
        //    if (Physics2D.gravity == six)
        //    {
        //        Physics2D.gravity = nine;
        //        moveHorizontal = false;
        //    }
        //    else if (Physics2D.gravity == nine)
        //    {
        //        Physics2D.gravity = twelve;
        //        moveHorizontal = true;
        //    }
        //    else if (Physics2D.gravity == twelve)
        //    {
        //        Physics2D.gravity = three;
        //        moveHorizontal = false;
        //    }
        //    else
        //    {
        //        Physics2D.gravity = six;
        //        moveHorizontal = true;
        //    }
        //    //Physics2D.gravity = new Vector2(Physics2D.gravity.x + 10, Physics2D.gravity.y + 10);
        //}

        //noOfTurns--;

        if (Physics2D.gravity == six && canGravity)
        {
            Physics2D.gravity = nine;
            moveHorizontal = false;
            canGravity = false;
            gravityButton.GetComponent<Button>().interactable = false;
            StartCoroutine(gravityCooldownFor(gravityCoolDown));
        }
        else if (Physics2D.gravity == nine && canGravity)
        {
            Physics2D.gravity = twelve;
            moveHorizontal = true;
            canGravity = false;
            gravityButton.GetComponent<Button>().interactable = false;
            StartCoroutine(gravityCooldownFor(gravityCoolDown));
        }
        else if (Physics2D.gravity == twelve && canGravity)
        {
            Physics2D.gravity = three;
            moveHorizontal = false;
            canGravity = false;
            gravityButton.GetComponent<Button>().interactable = false;
            StartCoroutine(gravityCooldownFor(gravityCoolDown));
        }
        else
        if(Physics2D.gravity == three && canGravity)
        {
            Physics2D.gravity = six;
            moveHorizontal = true;
            canGravity = false;
            gravityButton.GetComponent<Button>().interactable = false;
            StartCoroutine(gravityCooldownFor(gravityCoolDown));
        }
        //Physics2D.gravity = new Vector2(Physics2D.gravity.x + 10, Physics2D.gravity.y + 10);
    }

    private void FixedUpdate()
    {
        if (moveHorizontal)
        {
            if(Physics2D.gravity == six)
            {
                body.velocity = new Vector2(horizontal * runSpeed, 0);
                anim.SetFloat("moveSpeed", Mathf.Abs(body.velocity.x));
                if (body.velocity.x < 0)
                {
                    theSR.flipX = false; //by default kj drew the sprite facing the left direction so the cat is facing left
                }
                else if (body.velocity.x > 0)
                {
                    theSR.flipX = true;
                }
            }
            else
            {
                body.velocity = new Vector2(horizontal * runSpeed, 0);
                anim.SetFloat("moveSpeed", Mathf.Abs(body.velocity.x));
                if (body.velocity.x < 0)
                {
                    theSR.flipX = true; //by default kj drew the sprite facing the left direction so the cat is facing left
                }
                else if (body.velocity.x > 0)
                {
                    theSR.flipX = false;
                }
            }
           

        }
        else {
            if(Physics2D.gravity == nine)
            {
                body.velocity = new Vector2(0, vertical * runSpeed);
                anim.SetFloat("moveSpeed", Mathf.Abs(body.velocity.y));
                if (body.velocity.y < 0)
                {
                    theSR.flipX = true; //by default kj drew the sprite facing the left direction so the cat is facing left
                }
                else if (body.velocity.y > 0)
                {
                    theSR.flipX = false;
                }
            }
            else
            {
                body.velocity = new Vector2(0, vertical * runSpeed);
                anim.SetFloat("moveSpeed", Mathf.Abs(body.velocity.y));
                if (body.velocity.y < 0)
                {
                    theSR.flipX = false; //by default kj drew the sprite facing the left direction so the cat is facing left
                }
                else if (body.velocity.y > 0)
                {
                    theSR.flipX = true;
                }
            }
            
        }
    }

    private bool isGrounded() {
        if (Physics2D.gravity == six)
        {
            return Physics2D.Raycast(transform.position, Vector2.down, GetComponent<Collider2D>().bounds.extents.y + 0.1f);
        }
        else if (Physics2D.gravity == nine)
        {
            return Physics2D.Raycast(transform.position, Vector2.left, GetComponent<Collider2D>().bounds.extents.x + 0.1f);
        }
        else if (Physics2D.gravity == twelve)
        {
            return Physics2D.Raycast(transform.position, Vector2.up, GetComponent<Collider2D>().bounds.extents.y - 0.1f);
        }
        else
        {
            return Physics2D.Raycast(transform.position, Vector2.right, GetComponent<Collider2D>().bounds.extents.x - 0.1f);
        }

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Finish"))
        {
            if (SceneManager.GetActiveScene().buildIndex == 5)
            {
                SceneManager.LoadScene(0);
            }
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
        }

        if (collider.CompareTag("Laser"))
        {
            gameObject.SetActive(false);
            gameOver();
            // AdOnOrOff.instance.turnOffAdd();
            button.interactable = false;
            //Popup UI screen
        }
    }

    private void rotatePlayer()
    {
        if (Physics2D.gravity == nine && transform.rotation != new Quaternion(0.0f, 0.0f, -0.7f, 0.7f))
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, new Quaternion(0.0f, 0.0f, -0.7f, 0.7f), Time.deltaTime * 500);
        }
        if (Physics2D.gravity == twelve && transform.rotation != new Quaternion(0.0f, 0.0f, 1.0f, 0.0f))
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, new Quaternion(0.0f, 0.0f, 1.0f, 0.0f), Time.deltaTime * 500);
        }
        if (Physics2D.gravity == three && transform.rotation != new Quaternion(0.0f, 0.0f, 0.7f, 0.7f))
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, new Quaternion(0.0f, 0.0f, 0.7f, 0.7f), Time.deltaTime * 500);
        }
        if (Physics2D.gravity == six && transform.rotation != new Quaternion(0.0f, 0.0f, 0.0f, 1.0f))
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, new Quaternion(0.0f, 0.0f, 0.0f, 1.0f), Time.deltaTime * 500);
        }
    }

    private IEnumerator gravityCooldownFor(float timer) { 
        yield return new WaitForSeconds(timer);
        canGravity = true;
        gravityButton.GetComponent<Button>().interactable = true;
    }
}
