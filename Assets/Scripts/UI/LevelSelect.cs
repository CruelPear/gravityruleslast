using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelect : MonoBehaviour
{
    public GameObject[] levels;


    public void back()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
    public void loadLevelSelectScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("LevelSelect");
    }

    public void loadCredits()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Credits");
    }

    public void level1()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level1");
    }

    public void level2()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level2");
    }

    public void level3()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level3");
    }

    public void level4()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level4");
    }

    public void level5()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level5");
    }

    public void level6()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level6");
    }

    public void level7()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level7");
    }

    public void level8()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level8");
    }

    public void level9()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level9");
    }

    public void level10()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level10");
    }

}
