using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserScript : MonoBehaviour
{
    public float interval;
    private bool status;
    public GameObject laser;
    // Start is called before the first frame update
    void Start()
    {
        status = true;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(laserCountdown());
        Debug.Log("Quack");
    }

    IEnumerator laserCountdown() {
        if (status)
        {
            yield return new WaitForSeconds(interval);
            laser.SetActive(false);
            status = false;
        }
        else {
            yield return new WaitForSeconds(interval);
            laser.SetActive(true);
            status = true;
        }
    }
}
