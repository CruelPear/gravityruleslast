using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AdOnOrOff : MonoBehaviour
{
    public static AdOnOrOff instance;

    public Button button;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
    }

    public void turnOffAdd()
    {
        button.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
